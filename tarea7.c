#include <stdio.h>
int main(int argc, char const *argv[])

{
  int a, b;
  printf("\tPrograma que suma y resta dos números enteros\n\n");

  printf("Introduce el primer número entero: \n");
  scanf("%d", &a);
  printf("Introduce el segundo número entero: \n");
  scanf("%d", &b);

  printf("\nEl resultado de la suma es: %d", a+b);
  printf("\nEl resultado de la resta es: %d\n", a-b);

  return 0;
}
